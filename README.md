# Scoop Chromia

Welcome to the Scoop Chromia repository, your source for distributing Chromia CLI (CHR) through the [Scoop package manager](https://scoop.sh)

## How do I install these manifests?

After manifests have been committed and pushed, run the following:

```pwsh
scoop bucket add chromia https://gitlab.com/chromaway/core-tools/scoop-chromia/
scoop install chromia/chr
```

## How do I contribute new manifests?

To make a new manifest contribution, please read the [Contributing
Guide](https://github.com/ScoopInstaller/.github/blob/main/.github/CONTRIBUTING.md)
and [App Manifests](https://github.com/ScoopInstaller/Scoop/wiki/App-Manifests)
wiki page.
